package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
    WebDriver driver;

    public LoginPage(WebDriver driver){
        this.driver = driver;
    }
    /**
     * Locator
     */
    By userName = By.id("userName");
    By password = By.id("password");
    By loginBtn = By.id("login");

    /**
     * Method
     */
    public void enterUsername(String user){
        driver.findElement(userName).sendKeys(user);
    }
    public void enterPassword(String pass){
        driver.findElement(password).sendKeys(pass);
    }

    public void clickLoginBtn() {
        driver.findElement(loginBtn).click();
    }
}
