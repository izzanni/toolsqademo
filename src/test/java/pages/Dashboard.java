package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Dashboard {
    WebDriver driver;

    public Dashboard(WebDriver driver){
        this.driver = driver;
    }

    /**
     * Locator
     */
    By heading = By.className("main-header");
    By logoutBtn = By.id("submit");
    By searchField = By.id("searchBox");


    public String getHeading(){
        String head = driver.findElement(heading).getText();
        return head;
    }

    public void enterSearch(String searchValue){
        driver.findElement(searchField).sendKeys(searchValue);
    }

    public void clickLogoutBtn(){
        driver.findElement(logoutBtn).click();
    }


}
