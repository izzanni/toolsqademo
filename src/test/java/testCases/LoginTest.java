package testCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.Dashboard;
import pages.HomePage;
import pages.LoginPage;

public class LoginTest {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.manage().window().maximize();
        driver.get("https://demoqa.com/books");

        HomePage home = new HomePage(driver);
        LoginPage login = new LoginPage(driver);
        Dashboard dashboard = new Dashboard(driver);

        home.clickLoginBtn();

        login.enterUsername("gunjankaushik");
        login.enterPassword("Password@123");
        login.clickLoginBtn();

        Thread.sleep(3000);

        dashboard.getHeading();
        dashboard.enterSearch("Addy");

        Thread.sleep(5000);

        dashboard.clickLogoutBtn();

        Thread.sleep(3000);

        driver.quit();
    }
}
